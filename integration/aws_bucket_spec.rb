require "bucket_client/aws/aws_bucket"
require "bucket_client/aws/aws_client"
require "kirin_http"

describe BucketClient::AWSClient do

	http = KirinHttp::BasicClient.new
	id = ENV["AWS_ID"]
	secret = ENV["AWS_SECRET"]
	region = ENV["AWS_REGION"]
	client = BucketClient::AWSClient.new(http, id, secret, region)
	key = "kirin.bucket.client.client.integration"


	before(:all) {client.delete_bucket_if_exist key}
	after(:all) {client.delete_bucket_if_exist key}

	describe 'exist_bucket' do
		it 'should return false' do
			expect(client.exist_bucket key).to be_falsey
		end
	end

	describe 'delete_bucket_if_exist' do
		it 'should be successful' do
			result = client.delete_bucket_if_exist key
			expect(result.code).to eq 204
			expect(result.success).to be_truthy
			expect(result.message).to eq "Bucket already deleted"
		end
	end

	describe 'delete_bucket' do
		it 'should fail as bucket does not exist' do
			result = client.delete_bucket key
			expect(result.code).to eq 404
			expect(result.success).to be_falsey
			expect(result.value).to be_nil
		end
	end

	describe "set_read_policy" do
		it 'should fail as bucket does not exist' do
			result = client.set_read_policy key, :public
			expect(result.code).to eq 404
			expect(result.success).to be_falsey
			expect(result.value).to be_nil
		end
	end

	describe "set_get_cors" do
		it 'should fail as bucket does notexist' do
			result = client.set_get_cors key, ["*"]
			expect(result.code).to eq 404
			expect(result.success).to be_falsey
			expect(result.value).to be_nil
		end
	end

	describe "get_bucket!" do
		it 'should generate unsafe bucket with correct key' do
			bucket = client.get_bucket! key
			expect(bucket.key).to eq key
		end
	end

	describe "put_bucket" do
		it 'should create new bucket' do
			result = client.put_bucket key
			expect(result.code).to eq 200
			expect(result.success).to be_truthy
			expect(result.value.key).to eq key
			sleep(10)
		end
	end

	describe 'create_bucket' do
		it 'should fail creating a new bucket because it already exist' do

			expect(client.exist_bucket key).to be_truthy
			result = client.create_bucket key
			expect(result.code).to eq 409
			expect(result.success).to be_falsey
			expect(result.value).to be_nil
		end
	end

	describe 'delete_bucket' do
		it 'should succeed deleting a new bucket' do
			result = client.delete_bucket key
			expect(result.code).to eq 204
			expect(result.success).to be_truthy
		end
	end

	describe "create_bucket" do
		it 'should succeed at creating a new bucket' do
			result = client.create_bucket key
			expect(result.code).to eq 200
			expect(result.success).to be_truthy
			expect(result.value.key).to eq key
		end
	end

	describe 'put_bucket' do
		it 'should not fail even if bucket already exist' do
			result = client.put_bucket key
			expect(result.code).to eq 200
			expect(result.success).to be_truthy
			expect(result.value.key).to eq key
		end
	end

	describe "set_read_policy" do
		it 'should fail if access is neither :public or :private' do
			expect {client.set_read_policy key, :protected}.to raise_error(ArgumentError, "Read Policy not accepted")
		end
		it 'should succeed' do
			result = client.set_read_policy key, :public
			expect(result.code).to eq 204
			expect(result.success).to be_truthy
			expect(result.value).to be_nil
		end
	end

	describe "set_get_cors" do
		it 'should should succeed' do
			result = client.set_get_cors key, ["*"]
			expect(result.code).to eq 200
			expect(result.success).to be_truthy
			expect(result.value).to be_nil
		end
	end

	describe "delete_bucket_if_exist" do
		it 'should remove existing bucket' do
			result = client.delete_bucket_if_exist key
			expect(result.code).to eq 204
			expect(result.success).to be_truthy
			expect(result.value).to be_nil
			expect(client.exist_bucket key).to be_falsey
		end
	end

end