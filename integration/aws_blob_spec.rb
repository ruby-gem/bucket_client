require "bucket_client/aws/aws_bucket"
require "bucket_client/aws/aws_client"
require "kirin_http"
require "base64"

describe BucketClient::AWSBucket do
	http = KirinHttp::BasicClient.new
	id = ENV["AWS_ID"]
	secret = ENV["AWS_SECRET"]
	region = ENV["AWS_REGION"]
	client = BucketClient::AWSClient.new(http, id, secret, region)
	key = "kirin.bucket.client.bucket.integration"
	file = "img.jpg"
	bin = IO.binread "./integration/#{file}"
	before(:all) {client.put_bucket key}
	after(:all) {
		bucket = client.get_bucket key
		bucket.delete_blob_if_exist file
		client.delete_bucket_if_exist key
	}


	describe 'Blob Behaviour' do
		bucket = client.get_bucket! key
		describe 'exist_blob' do
			it 'should return false as blob does not exist' do
				expect(bucket.exist_blob file).to be_falsey
			end
		end

		describe 'delete_blob_if_exist' do
			it 'should succeed' do
				result = bucket.delete_blob_if_exist file
				expect(result.code).to eq 204
				expect(result.success).to be_truthy
			end
		end

		describe 'delete_blob' do
			it 'should fail as blob does not exist' do
				result = bucket.delete_blob file
				expect(result.success).to be_falsey
			end
		end

		describe 'update_blob' do
			it 'should fail as blob does not exist' do
				result = bucket.update_blob bin, file
				expect(result.success).to be_falsey
			end
		end

		describe 'get_blob' do
			it 'should fail as blob does not exist' do
				result = bucket.get_blob file
				expect(result.success).to be_falsey
			end
		end

		describe "put_blob" do
			it 'should succeed' do
				result = bucket.put_blob bin, file
				expect(result).to be_truthy
				expect(result.value).to eq "https://s3-#{region}.amazonaws.com/#{key}/#{file}"
			end

			it 'should succeed on MD files' do
				f = "readme.md"
				readme = "IyBSZWFkIG1lDQpISSBISSE="
				readme_bin = Base64.decode64 readme
				result = bucket.put_blob readme_bin, f
				expect(result).to be_truthy
				expect(result.value).to eq "https://s3-#{region}.amazonaws.com/#{key}/#{f}"
			end

		end

		describe 'exist_blob' do
			it 'should exist' do
				expect(bucket.exist_blob file).to be_truthy
			end
		end

		describe "create_blob" do
			it 'should fail as blob already exist' do
				result = bucket.create_blob bin, file
				expect(result.success).to be_falsey
			end
		end

		describe "get_blob" do
			it 'should obtain the binary of the blob' do
				result = bucket.get_blob file
				expect(result.success).to be_truthy
				expect(result.value).to eq bin
			end
		end

		describe "delete_blob" do
			it 'should pass as blob exist' do
				result = bucket.delete_blob file
				expect(result.success).to be_truthy
			end

			it 'should delete MD file too' do
				f = "readme.md"
				result = bucket.delete_blob f
				expect(result.success).to be_truthy
			end

		end

		describe "create_blob" do
			it 'should work as blob does not exist' do
				result = bucket.create_blob bin, file
				expect(result.success).to be_truthy
				expect(result.value).to eq "https://s3-#{region}.amazonaws.com/#{key}/#{file}"
			end
		end

		describe "update_blob" do
			it 'should work as blob exist' do
				result = bucket.update_blob bin, file
				expect(result.success).to be_truthy
				expect(result.value).to eq "https://s3-#{region}.amazonaws.com/#{key}/#{file}"
			end
		end

		describe "put_blob" do
			it 'should work as blob exist' do
				result = bucket.put_blob bin, file
				expect(result.success).to be_truthy
				expect(result.value).to eq "https://s3-#{region}.amazonaws.com/#{key}/#{file}"
			end
		end

		describe "delete_blob_if_exist" do
			it 'should delete blob' do
				result = bucket.delete_blob_if_exist file
				expect(result.success).to be_truthy
			end
		end

		describe "exist_blob" do
			it 'should no longer exist' do
				expect(bucket.exist_blob file).to be_falsey
			end
		end

	end

end