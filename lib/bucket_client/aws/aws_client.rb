require "bucket_client/client"
require "bucket_client/aws4_request_signer"
require "bucket_client/aws/aws_http_client"
require "bucket_client/aws/aws_policy_factory"
require "bucket_client/aws/aws_bucket"
require "bucket_client/aws/aws_http_client"
require "bucket_client/operation_result"
require "mimemagic"
require "json"

module BucketClient
	class AWSClient < Client

		# @param [KirinHttp::Client] client Basic http client
		# @param [String] id AWS id
		# @param [String] secret AWS secret
		# @param [String] region Region for bucket
		def initialize(client, id, secret, region)
			signer = AWS4RequestSigner.new(id, secret)
			@region = region
			@http = AWSHttpClient.new(signer, region, client)
			@policy_factory = AWSPolicyFactory.new
		end

		def exist_bucket(key)
			resp = @http.query(:head, "https://s3-#{@region}.amazonaws.com/#{key}")
			resp.code == 200
		end

		def get_bucket!(key)
			AWSBucket.new(@region, @http, self, key)
		end

		def put_bucket(key)
			exist = exist_bucket key
			if exist
				bucket = get_bucket! key
				OperationResult.new(true, "OK", bucket, 200)
			else
				create_bucket key
			end
		end

		def create_bucket(key)
			content = "<CreateBucketConfiguration xmlns='http://s3.amazonaws.com/doc/2006-03-01/'>
                                    <LocationConstraint>#{@region}</LocationConstraint>
                           </CreateBucketConfiguration>"
			endpoint = "https://s3-#{@region}.amazonaws.com/#{key}"
			resp = @http.query(:put, endpoint, content)
			success = resp.code === 200
			if success
				bucket = get_bucket! key
				OperationResult.new(success, "Bucket created", bucket, 200)
			else
				OperationResult.new(success, resp.content, nil, resp.code)
			end
		end

		def delete_bucket(key)
			endpoint = "https://s3-#{@region}.amazonaws.com/#{key}"
			resp = @http.query(:delete, endpoint)
			success = resp.code === 204
			if success
				OperationResult.new(success, "Bucket deleted", nil, resp.code)
			else
				OperationResult.new(success, resp.content, nil, resp.code)
			end
		end

		def delete_bucket_if_exist(key)
			exist = exist_bucket key
			if exist
				delete_bucket key
			else
				OperationResult.new(true, "Bucket already deleted", nil, 204)
			end
		end

		def set_read_policy(key, access)
			raise ArgumentError.new("Read Policy not accepted") if access != :public && access != :private
			resp = set_policy key, access, 10
			OperationResult.new(resp.code === 204, resp.content, nil, resp.code)
		end

		def set_get_cors(key, cors)
			resp = set_cors key, cors, 10
			OperationResult.new(resp.code === 200, resp.content, nil, resp.code)
		end

		def get_blob(uri)
			resp = @http.query(:get, uri)
			success = resp.code === 200
			if success
				OperationResult.new(success, "OK", resp.content, resp.code)
			else
				OperationResult.new(success, resp.content, nil, resp.code)
			end
		end

		def exist_blob(uri)
			@http.query(:head, uri).code === 200
		end

		def put_blob(payload, uri)
			mime = MimeMagic.by_magic payload
			type = mime&.type || "text/plain"
			resp = @http.query(:put, uri, payload, type, "text/plain")
			success = resp.code === 200
			if success
				OperationResult.new(success, "Blob put", uri, resp.code)
			else
				OperationResult.new(success, resp.content, nil, resp.code)
			end
		end

		def update_blob(payload, uri)
			exist = exist_blob uri
			if exist
				put_blob payload, uri
			else
				OperationResult.new(false, "Blob does not exist", nil, 404)
			end
		end

		def delete_blob_if_exist(uri)
			resp = @http.query(:delete, uri)
			success = resp.code === 204
			if success
				OperationResult.new(success, "Blob deleted", nil, resp.code)
			else
				OperationResult.new(success, resp.content, nil, resp.code)
			end
		end

		def delete_blob(uri)
			exist = exist_blob uri
			if exist
				delete_blob_if_exist uri
			else
				OperationResult.new(false, "Blob does not exist", nil, 404)
			end
		end

		private

			def set_policy(key, access, max, count = 0)
				failure = "Failed too many times due to conflict"
				return OperationResult.new(false, failure, nil, 400) if ++count === max
				endpoint = "https://s3-#{@region}.amazonaws.com/#{key}/?policy="
				if access === :public
					policy = @policy_factory.generate_policy access, key
					resp = @http.query(:put, endpoint, policy.to_json, "application/json")
				else
					resp = @http.query :delete, endpoint
				end
				if resp.code === 409
					set_policy key, access, max, count
				else
					resp
				end
			end

			def set_cors(key, cors, max, count = 0)
				failure = "Failed too many times due to conflict"
				return OperationResult.new(false, failure, nil, 400) if ++count === max
				endpoint = "https://s3-#{@region}.amazonaws.com/#{key}/?cors="
				if cors.length > 0
					xml = cors_xml cors
					resp = @http.query :put, endpoint, xml
				else
					resp = @http.query :delete, endpoint
				end
				if resp.code === 409
					set_cors key, cors, max, count
				else
					resp
				end
			end

			def cors_xml(cors)
				rules = cors.map(&method(:cors_rule))
				"<CORSConfiguration>#{rules.join "\n"}</CORSConfiguration>"
			end

			def cors_rule(cors)
				"<CORSRule>
		       		<AllowedOrigin>#{cors}</AllowedOrigin>
		       		<AllowedMethod>GET</AllowedMethod>
		       		<AllowedHeader>*</AllowedHeader>
		       		<MaxAgeSeconds>3000</MaxAgeSeconds>
		     	</CORSRule>"
			end

	end
end

