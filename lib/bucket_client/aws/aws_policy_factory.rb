require 'securerandom'

class AWSPolicyFactory
	def generate_policy(access, key)
		statements = []
		if access === :public
			# uuid_2 = "69cab402-0674-40e6-9915-982b92016d6a"
			uuid_2 = SecureRandom.uuid.to_s
			policy_statement = {
				"Sid": "AllowPublicRead#{uuid_2}",
				"Action": ["s3:GetObject"],
				"Effect": "Allow",
				"Resource": "arn:aws:s3:::#{key}/*",
				"Principal": "*"
			}
			statements.push policy_statement
		end
		uuid_1 = SecureRandom.uuid.to_s
		# uuid_1 = "668c8c5d-3efb-458d-bebb-6fa194b55732"
		{
			"Id": "ReadPolicy#{uuid_1}",
			"Version": "2012-10-17",
			"Statement": statements
		}
	end
end