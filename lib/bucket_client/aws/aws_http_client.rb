require "kirin_http"

module BucketClient
	class AWSHttpClient

		# @param [Client::AWS4RequestSigner] signer aws4 signer
		# @param [String] region region of the service
		# @param [KirinHttp::Client] http Http client to send http Message
		def initialize(signer, region, http)
			@signer = signer
			@region = region
			@http = http
		end

		# @param [Symbol] method
		# @param [String] endpoint
		# @param [Object] content
		# @param [String] type
		# @return [KirinHttp::Response]
		def query(method, endpoint, content = nil, type = "text/plain", accept = nil)
			accept = type if accept.nil?
			header = {
				"Content-Type": type,
				"Accept": accept
			}
			message = KirinHttp::Message.new(endpoint, method, content, header)
			message = @signer.sign message, "s3", @region
			@http.send message
		end

	end
end
