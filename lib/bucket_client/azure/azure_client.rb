require "azure/storage/blob"
require "addressable/uri"
require "bucket_client/azure/azure_bucket"
require "bucket_client/operation_result"
require "mimemagic"
require "json"

module BucketClient
	class AzureClient < Client

		def initialize(account, secret)
			@client = Azure::Storage::Blob::BlobService.create(
				storage_account_name: account,
				storage_access_key: secret
			)
			@acc = account
		end

		def exist_bucket(key)
			begin
				@client.get_container_metadata key
				true
			rescue Azure::Core::Http::HTTPError => e
				if e.status_code === 404
					false
				else
					raise e
				end
			end
		end

		def get_bucket!(key)
			AzureBucket.new(self, key, @client, @acc)
		end

		def put_bucket(key)
			exist = exist_bucket key
			if exist
				bucket = get_bucket! key
				OperationResult.new(true, "OK", bucket, 200)
			else
				create_bucket key
			end
		end

		def create_bucket(key)
			exist = exist_bucket key
			return OperationResult.new(false, "Bucket already exist", nil, 409) if exist
			begin
				@client.create_container key
				value = get_bucket! key
				OperationResult.new(true, "Azure Container created", value, 200)
			rescue Azure::Core::Http::HTTPError => e
				if e.status_code === 409
					sleep(3)
					create_bucket key
				else
					OperationResult.new(false, e.description, nil, e.status_code)
				end

			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def delete_bucket(key)
			begin
				@client.delete_container key
				OperationResult.new(true, "Container deleted", nil, 200)
			rescue Azure::Core::Http::HTTPError => e
				OperationResult.new(false, e.description, nil, e.status_code)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def delete_bucket_if_exist(key)
			exist = exist_bucket key
			if exist
				delete_bucket key
			else
				OperationResult.new(true, "Bucket already deleted", nil, 200)
			end
		end

		def set_read_policy(key, access)
			raise ArgumentError.new("Read Policy not accepted") if access != :public && access != :private
			begin
				level = access === :public ? "blob" : ""
				@client.set_container_acl key, level
				OperationResult.new(true, "OK", nil, 200)
			rescue Azure::Core::Http::HTTPError => e
				OperationResult.new(false, e.description, nil, e.status_code)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def set_get_cors(key, cors)
			begin
				cors_rule = Azure::Storage::BlobService::CorsRule.new
				cors_rule.allowed_origins = cors
				cors_rule.allowed_methods = ["GET"]
				cors_rule.allowed_headers = ['*']
				cors_rule.exposed_headers = ['*']
				service_properties = Azure::Storage::Blob::StorageServiceProperties.new
				service_properties.cors.cors_rules = [cors_rule]
				@client.set_service_properties(service_properties)
				OperationResult.new(true, "OK", nil, 200)
			rescue Azure::Core::Http::HTTPError => e
				OperationResult.new(false, e.description, nil, e.status_code)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def get_blob(uri)
			data = break_uri uri
			begin
				_, content = @client.get_blob data[:bucket], data[:blob]
				OperationResult.new(true, "", content, 200)
			rescue Azure::Core::Http::HTTPError => e
				OperationResult.new(false, e.description, nil, e.status_code)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def exist_blob(uri)
			begin
				data = break_uri uri
				@client.get_blob_metadata data[:bucket], data[:blob]
				true
			rescue Azure::Core::Http::HTTPError => e
				if e.status_code === 404
					false
				end
			end
		end

		def put_blob(payload, uri)
			mime = MimeMagic.by_magic payload
			data = break_uri uri
			begin
				type = mime&.type || "text/plain"
				@client.create_block_blob data[:bucket], data[:blob], payload, {:content_type => type}
				OperationResult.new(true, "OK", uri, 204)
			rescue Azure::Core::Http::HTTPError => e
				OperationResult.new(false, e.description, nil, e.status_code)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def update_blob(payload, uri)
			exist = exist_blob uri
			if exist
				put_blob payload, uri
			else
				OperationResult.new(false, "Blob does not exist", nil, 404)
			end
		end

		def delete_blob_if_exist(uri)
			exist = exist_blob uri
			if exist
				delete_blob uri
			else
				OperationResult.new(true, "Blob already deleted", nil, 204)
			end
		end

		def delete_blob(uri)
			data = break_uri uri
			begin
				@client.delete_blob(data[:bucket], data[:blob])
				OperationResult.new(true, "Deleted Blob", nil, 204)
			rescue Azure::Core::Http::HTTPError => e
				OperationResult.new(false, e.description, nil, e.status_code)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		private

			def break_uri(uri)
				url = Addressable::URI.parse uri
				fragments = url.path.split("/").select {|x| !x.nil? && !x.empty?}
				bucket = fragments[0]
				blob = fragments.drop(1).join "/"
				{bucket: bucket, blob: blob}
			end

	end
end


