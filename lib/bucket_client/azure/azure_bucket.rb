require "bucket_client/operation_result"

module BucketClient
	class AzureBucket < Bucket

		attr_reader :key

		# @param [AzureClient] parent
		# @param [String] key
		# @param [Azure::Storage::Blob::BlobService] client
		def initialize(parent, key, client, master)
			@bucket_client = parent
			@client = client
			@key = key
			@master = master
		end

		def get_uri(key)
			"https://#{@master}.blob.core.windows.net/#{@key}/#{key}"
		end

		def get_blob_with_uri(uri)
			@bucket_client.get_blob uri
		end

		def exist_blob_with_uri(uri)
			@bucket_client.exist_blob(uri)
		end

		def put_blob_with_uri(payload, uri)
			@bucket_client.put_blob payload, uri
		end

		def update_blob_with_uri(payload, uri)
			exist = exist_blob_with_uri uri
			if exist
				put_blob_with_uri payload, uri
			else
				OperationResult.new false, "Blob does not exist", nil, 400
			end
		end

		def delete_blob_with_uri(uri)
			@bucket_client.delete_blob(uri)
		end

		def delete_blob_if_exist_with_uri(uri)
			@bucket_client.delete_blob_if_exist(uri)
		end

		def get_blob(key)
			get_blob_with_uri(get_uri key)
		end

		def exist_blob(key)
			exist_blob_with_uri(get_uri key)
		end

		def put_blob(payload, key)
			put_blob_with_uri(payload, get_uri(key))
		end

		def create_blob(payload, key)
			exist = exist_blob key
			if exist
				OperationResult.new false, "Blob already exist", nil, 400
			else
				put_blob payload, key
			end
		end

		def update_blob(payload, key)
			update_blob_with_uri(payload, get_uri(key))
		end

		def delete_blob(key)
			delete_blob_with_uri(get_uri key)
		end

		def delete_blob_if_exist(key)
			delete_blob_if_exist_with_uri(get_uri key)
		end

	end
end