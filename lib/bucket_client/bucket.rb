require "bucket_client/bucket_operation_exception"

module BucketClient
	module KeyMethod
		# Get blob as byte array
		#
		# result = bucket.get_blob "image.png"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> the byte array of the blob
		#
		# @param [String] key the blob id or name
		# @return [OperationResult]
		def get_blob(key)
			raise NotImplementedError key
		end

		# Get blob as byte array
		#
		# Raises exception if the operation fails.
		#
		# img = bucket.get_blob! "image.png"
		#
		# @param [String] key the blob id or name
		# @return [Array<Byte>]
		def get_blob!(key)
			result = get_blob key
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Check if the blob exist
		#
		# Raises exception if operation fails
		#
		# exist = bucket.exist_blob "image.png" #=> true if exist, false if not
		#
		# @param [String] key the blob id or name
		# @return [Boolean]
		def exist_blob(key)
			raise NotImplementedError key
		end

		# Create blob with payload
		# Fails if blob already exist
		#
		# img = IO.binread "image.png"
		# result = bucket.create_blob img, "image.png"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> URI of the blob
		#
		# @param [Array<Byte>] payload the payload to create blob with
		# @param [String] key the blob id or name
		# @return [OperationResult]
		def create_blob(payload, key)
			raise NotImplementedError payload.to_s, key
		end

		# Creates blob with payload
		# Fails if blob already exist
		#
		# Raises exception if operation fails
		#
		# img = IO.binread "image.png"
		# uri = bucket.create_blob! img, "image.png" #=> URI of the created blob
		#
		# @param [Array<Byte>] payload the payload to create blob with
		# @param [String] key the blob id or name
		# @return [String]
		def create_blob!(payload, key)
			result = create_blob payload, key
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Updates the blob with new payload
		# Fails if blob does not exist
		#
		# img = IO.binread "image.png"
		# result = bucket.update_blob img, "image.png"
		# result.success #=> whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> URI of the blob
		#
		# @param [Array<Byte>] payload the payload to update
		# @param [String] key the blob id or name
		# @return [OperationResult]
		def update_blob(payload, key)
			raise NotImplementedError payload.to_s, key
		end

		# Updates the blob with new payload
		# Fails if blob does not exist
		# Raises exception if operation fails
		#
		# img = IO.binread "image.png"
		# result = bucket.update_blob!(img, "image.png") #=> URI of updated blob
		#
		# @param [Array<Byte>] payload the payload to update
		# @param [String] key the blob id or name
		# @return [String]
		def update_blob!(payload, key)
			result = update_blob payload, key
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Creates a new blob with payload if blob does not exist
		# Updates blob with new payload if blob exist
		#
		# img = IO.binread "image.png"
		# result = bucket.put_blob(img, "image.png")
		# result.success #=> whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> URI of the blob
		#
		# @param [Array<Byte>] payload the payload to put
		# @param [String] key the blob id or name
		# @return [OperationResult]
		def put_blob(payload, key)
			raise NotImplementedError payload.to_s, key
		end

		# Creates a new blob with payload if blob does not exist
		# Updates blob with new payload if blob exist
		#
		# Raises exception if operation fails
		#
		# img = IO.binread "image.png"
		# uri = bucket.put_blob! img, "image.png" #=> uri of the blob
		#
		# @param [Array<Byte>] payload the payload to put
		# @param [String] key the blob id or name
		# @return [String]
		def put_blob!(payload, key)
			result = put_blob payload, key
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Deletes a blob
		# Fails if the blob does not exist. To prevent this behaviour, use
		# delete_blob_if_exist method
		#
		# result = bucket.delete_blob "image.png"
		# result.success #=> whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> nil
		#
		# @param [String] key the blob id or name
		# @return [OperationResult]
		def delete_blob(key)
			raise NotImplementedError key
		end

		# Deletes a blob
		# Fails if the blob does not exist. To prevent this behaviour, use
		# delete_blob_if_exist method
		#
		# Raises exception if the operation fails
		#
		# bucket.delete_blob! "image.png"
		# @param [String] key the blob id or name
		def delete_blob!(key)
			result = delete_blob key
			raise BucketOperationException.new(result) unless result.success
		end

		# Deletes a blob if it exist
		#
		# result = bucket.delete_blob_if_exist "image.png"
		# result.success #=> whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> nil
		#
		# @param [String] key the blob id or name
		# @return [OperationResult]
		def delete_blob_if_exist(key)
			raise NotImplementedError key
		end

		# Deletes a blob if it exist
		# Raises exception if operation fails
		#
		# bucket.delete_blob_if_exist! "image.png"
		#
		# @param [String] key the blob id or name
		def delete_blob_if_exist!(key)
			result = delete_blob_if_exist key
			raise BucketOperationException.new(result) unless result.success
		end
	end

	module UriMethod
		# Gets the blob in the target URL as byte array.
		#
		# blob = bucket.get_blob_with_uri "https://host.com/bucket"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> payload of the blob as byte array
		#
		# @param [String] uri the endpoint of the blob you want to get
		# @return [OperationResult]
		def get_blob_with_uri(uri)
			raise NotImplementedError uri
		end

		# Gets the blob in the target URL as byte array
		#
		# Raises exception if operation fails
		#
		# blob = bucket.get_blob_with_uri! "https://host.com/bucket" #=> [12,65,127,33] (some byte array)
		#
		# @param [String] uri the endpoint of the blob you want to get
		# @return [Array<Byte>]
		def get_blob_with_uri!(uri)
			result = get_blob_with_uri(uri)
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Check if blob exist
		#
		# Raises exception if the operation fails
		#
		# exist = bucket.exist_blob_with_uri "https://host.com/folder/blob.ext"
		# exist #=> true if exist, false if does not exist
		#
		# @param [String] uri the URL of the blob
		# @return [Boolean]
		def exist_blob_with_uri(uri)
			raise NotImplementedError uri
		end

		# Updates a blob with new payload in byte array
		# Fails if blob with the URI doesn't exist
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = bucket.update_blob_with_uri img, uri
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> Uri of update blob
		#
		# @param [Array<Byte>] payload the payload to update the blob
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def update_blob_with_uri(payload, uri)
			raise NotImplementedError payload.to_s + uri
		end

		# Updates a blob with new payload in byte array
		# Fails if blob with the URI doesn't exist
		# Raises exception when fails
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = bucket.update_blob_with_uri!(img, uri) #=> URI of update blob
		#
		# @param [Array<Byte>] payload the payload to update the blob
		# @param [String] uri the URL of the blob
		def update_blob_with_uri!(payload, uri)
			result = update_blob_with_uri payload, uri
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Creates the blob with the payload if it does not exist
		# Updates the blob with the new payload if it exist
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = bucket.put_blob_with_uri img, uri
		# result.success #=> whether the operation succeeded
		# result.code #=> Status code of the operation
		# result.message #=> Error message if the operation failed
		# result.value #=> Uri of blob
		#
		# @param [Array<Byte>] payload the payload to put
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def put_blob_with_uri(payload, uri)
			raise NotImplementedError payload.to_s + uri
		end

		# Creates the blob with the payload if it does not exist,
		# Updates the blob with the new payload if it exist
		#
		# Raises exception if the operation fails
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = bucket.put_blob_with_uri!(img,uri) #=> returns URI of updated blob
		#
		# @param [Array<Byte>] payload the payload to put
		# @param [String] uri the URL of the blob
		# @return [String]
		def put_blob_with_uri!(payload, uri)
			result = put_blob_with_uri payload, uri
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Deletes the blob in the provided URI
		# Fails if the blob does not exist. Use delete_blob_if_exist if you
		# do not want this behaviour
		#
		# uri = "https://host.com/folder/pic.png"
		# result = bucket.delete_blob_with_uri uri
		# result.success #=> whether the operation succeeded
		# result.code #=> Status code of the operation
		# result.message #=> Error message if the operation failed
		# result.value #=> nil
		#
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def delete_blob_with_uri(uri)
			raise NotImplementedError uri
		end

		# Deletes the blob in the provided URI
		# Fails if the blob does not exist. Use delete_blob_if_exist if you
		# do not want this behaviour
		#
		# Raises exception if the operation fails
		#
		# uri = "https://host.com/folder/pic.png"
		# bucket.delete_blob_with_uri! uri
		#
		# @param [String] uri URL of the blob
		def delete_blob_with_uri!(uri)
			result = delete_blob_with_uri uri
			raise BucketOperationException.new(result) unless result.success
		end

		# Deletes the blob if it exist, else does nothing
		#
		# uri = "https://host.com/folder/pic.png"
		# result = bucket.delete_blob_with_uri uri
		# result.success #=> whether the operation succeeded
		# result.code #=> Status code of the operation
		# result.message #=> Error message if the operation failed
		# result.value #=> nil
		#
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def delete_blob_if_exist_with_uri(uri)
			raise NotImplementedError uri
		end

		# Deletes the blob if it exist, else does nothing
		# Raises exception if the operation fails
		#
		# uri = "https://host.com/folder/pic.png"
		# bucket.delete_blob_with_uri! uri
		#
		# @param [String] uri the URL of the blob
		def delete_blob_if_exist_with_uri!(uri)
			result = delete_blob_if_exist_with_uri uri
			raise BucketOperationException.new(result) unless result.success
		end
	end

	class Bucket

		include KeyMethod, UriMethod

		# Obtains the URI of the blob from key of the blob
		# Does not raise exception even if blob does not exist
		#
		# uri = bucket.get_uri("image.png") #=> https://host.com/bucket/image.png
		#
		# @param [String] key the blob id or name
		# @return [String]
		def get_uri(key)
			raise NotImplementedError key
		end
	end
end