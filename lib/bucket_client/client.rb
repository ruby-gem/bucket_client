module BucketClient
	module BucketMethods
		# Check if bucket exist
		# Throws exception if the HTTP request fails
		#
		# exist = client.exist_bucket("folder") # => true (if bucket exist)
		# exist = client.exist_bucket("folder") #=> false (if bucket does not exist)
		#
		# @param [String] key the bucket name or id
		# @return [Boolean]
		def exist_bucket(key)
			raise NotImplementedError key
		end

		# Creates a bucket
		# Fails if bucket already exist
		#
		# result = client.create_bucket "folder"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> the bucket instance
		#
		# @param [String] key the bucket name or id
		# @return [OperationResult]
		def create_bucket(key)
			raise NotImplementedError key
		end

		# Creates a bucket
		# Fails if bucket already exist
		#
		# Raises exception if the operation failed
		#
		# bucket = client.create_bucket!("folder_name")
		# bucket #=> Bucket Instance
		#
		# @param [String] key the bucket name or id
		# @return [Bucket]
		def create_bucket!(key)
			result = create_bucket key
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Deletes a bucket
		# Fails if the bucket does not exist
		#
		# result = client.delete_bucket "folder"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> nil
		#
		# @param [String] key the bucket name or id
		# @return [OperationResult]
		def delete_bucket(key)
			raise NotImplementedError key
		end

		# Deletes a bucket
		# Fails if the bucket does not exist
		# Raises error if it operation fails
		#
		# client.delete_bucket! "folder"
		#
		# @param [String] key the bucket name or id
		def delete_bucket!(key)
			result = delete_bucket key
			raise BucketOperationException.new(result) unless result.success
		end

		# Sets the read policy of the bucket
		# Fails if bucket does not exist
		#
		# Raises exception if neither :public nor :private is provided.
		#
		# result = client.set_read_policy :public #=> Sets read policy to public
		# result_2 = client.set_read_policy :private #=> Sets read policy to private
		#
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> nil
		#
		# @param [String] key the bucket name or id
		# @param [Symbol] access :public or :private
		# @return [OperationResult]
		def set_read_policy(key, access)
			raise NotImplementedError key + access
		end

		# Sets the read policy of the bucket
		# Fails if bucket does not exist
		#
		# Raises exception if neither :public nor :private is provided.
		# Raises exception if the operation fails
		#
		# client.set_read_policy! :public  #=> Sets read policy to public
		# client.set_read_policy! :private  #=> Sets read policy to private
		#
		# @param [String] key the bucket name or id
		# @param [Symbol] access :public or :private
		def set_read_policy!(key, access)
			result = set_read_policy key, access
			raise BucketOperationException.new(result) unless result.success
		end

		# Sets the GET CORS for the bucket
		# Fails if bucket does not exist
		#
		# result = client.set_get_cors "folder", ["domain2.net","domain1.net"]
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> nil
		#
		# @param [String] key the bucket name or id
		# @param [Array<String>] cors Array of CORS to allow GET requests
		# @return [OperationResult]
		def set_get_cors(key, cors)
			throw new NotImplementedError key + cors.to_s
		end

		# Sets the GET CORS for the bucket
		# Fails if bucket does not exist
		#
		# Raises exception if operation fails
		#
		# client.set_get_cors! "folder", ["domain2.net","domain1.net"]
		#
		# @param [String] key the bucket name or id
		# @param [Array<String>] cors Array of CORS to allow GET requests
		def set_get_cors!(key, cors)
			result = set_get_cors key, cors
			raise BucketOperationException.new(result) unless result.success
		end

		# Creates the bucket if it does not exist
		#
		# result = client.put_bucket "folder"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> bucket instance
		#
		# @param [String] key the bucket name or id
		# @return [OperationResult]
		def put_bucket(key)
			raise NotImplementedError key
		end

		# Creates the bucket if it does not exist
		#
		# Raises exception if the operation fails
		#
		# bucket = client.put_bucket "folder"! #=> Bucket if succeeds
		#
		# @param [String] key the bucket name or id
		# @return [Bucket]
		def put_bucket!(key)
			result = put_bucket key
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Delete the bucket if it exist
		#
		# result = client.delete_bucket_if_exist "folder"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# bucket.value #=> nil
		#
		# @param [String] key the bucket name or id
		# @return [OperationResult]
		def delete_bucket_if_exist(key)
			raise NotImplementedError key
		end

		# Delete the bucket if it exist
		#
		# Raises exception if operations fails
		#
		# client.delete_bucket_if_exist! "folder"
		#
		# @param [String] key the bucket name or id
		def delete_bucket_if_exist!(key)
			result = delete_bucket key
			raise BucketOperationException.new(result) unless result.success
		end

		# Gets the instance of the bucket.
		#
		# Checks whether the bucket exist and returns a Bucket Instance to interact
		# with the bucket if it does.
		#
		# Raises exception if bucket does not exist
		#
		# bucket = client.get_bucket "folder" #=> Gets bucket instance of it exist
		#
		# @param [String] key the bucket name or id
		# @return [Bucket]
		def get_bucket(key)
			exist = exist_bucket key
			result = OperationResult.new(false, "Bucket does not exist", nil, 404)
			raise BucketOperationException.new(result) unless exist
			get_bucket! key
		end

		# Gets the instance of the bucket
		# Does not check whether the bucket exist or not. Simply generates a bucket instance
		# optimistically. This operation is non-block as compared to get_bucket as it does
		# not send out a HTTP request to check if the bucket exist.
		#
		# However, the bucket will not work (raise exception) if the bucket does not exist
		# when you use the instance. Use this method only when you are sure that the bucket
		# exist.
		#
		# bucket = client.get_bucket! "folder"
		#
		# @param [String] key the bucket name or id
		# @return [Bucket]
		def get_bucket!(key)
			raise NotImplementedError key
		end
	end

	module BlobMethods
		# Gets the blob in the target URL as byte array.
		#
		# blob = client.get_blob "https://host.com/bucket"
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> Byte array (payload of blob)
		#
		# @param [String] uri the endpoint of the blob you want to get
		# @return [OperationResult]
		def get_blob(uri)
			raise NotImplementedError uri
		end

		# Gets the blob in the target URL as byte array
		#
		# Raises exception if operation fails
		#
		# blob = client.get_blob! "https://host.com/bucket" #=> [12,65,127,33] (some byte array)
		#
		# @param [String] uri the endpoint of the blob you want to get
		# @return [Array<Byte>]
		def get_blob!(uri)
			result = get_blob(uri)
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Check if blob with URI exist#
		#
		# Raises exception when the operation fails
		#
		# exist = exist_blob "https://host.com/folder/blob.ext"
		# exist #=> true if exist, false if does not exist
		#
		# @param [String] uri the URL of the blob
		# @return [Boolean]
		def exist_blob(uri)
			raise NotImplementedError uri
		end

		# Updates a blob with new payload in byte array
		# Fails if blob with the URI doesn't exist
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = client.update_blob img, uri
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> Uri of update blob
		#
		# @param [Array<Byte>] payload the payload to update the blob
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def update_blob(payload, uri)
			raise NotImplementedError payload.to_s + uri
		end

		# Updates a blob with new payload in byte array
		# Fails if blob doesnt exist
		#
		# Raises exception if operation fails
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = client.update_blob! img, uri #=> URI of update blob
		#
		# @param [Array<Byte>] payload the payload to update the blob
		# @param [String] uri the URL of the blob
		def update_blob!(payload, uri)
			result = update_blob payload, uri
			raise BucketOperationException.new(result) unless result.success
			result.value
		end

		# Creates the blob with the payload if it does not exist,
		# Updates the blob with the new payload if it exist
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = client.put_blob img, uri
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> Uri of blob
		#
		# @param [Array<Byte>] payload the payload to put
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def put_blob(payload, uri)
			raise NotImplementedError payload.to_s + uri
		end

		# Creates the blob with the payload if it does not exist,
		# Updates the blob with the new payload if it exist
		#
		# Raises exception if the operation fails
		#
		# img = IO.binread "pic.png"
		# uri = "https://host.com/folder/pic.png"
		# result = client.put_blob!(img,uri) #=> returns URI of updated blob
		#
		# @param [Array<Byte>] payload the payload to put
		# @param [String] uri the URL of the blob
		# @return [String]
		def put_blob!(payload, uri)
			result = put_blob payload, uri
			raise NotImplementedError result unless result.success
			result.value
		end

		# Deletes the blob in the provided URI
		# Fails if the blob does not exist. Use delete_blob_if_exist if you
		# do not want this behaviour
		#
		# uri = "https://host.com/folder/pic.png"
		# result = client.delete_blob uri
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> nil
		#
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def delete_blob(uri)
			raise NotImplementedError uri
		end

		# Deletes the blob in the provided URI
		# Fails if the blob does not exist. Use delete_blob_if_exist if you
		# do not want this behaviour
		#
		# Raises exception if the operation fails
		#
		# uri = "https://host.com/folder/pic.png"
		# client.delete_blob! uri
		#
		# @param [String] uri URL of the blob
		def delete_blob!(uri)
			result = delete_blob uri
			raise BucketOperationException.new(result) unless result.success
		end

		# Deletes the blob if it exist, else does nothing
		#
		# uri = "https://host.com/folder/pic.png"
		# result = client.delete_blob uri
		# result.success #=> Whether the operation succeeded
		# result.code #=> Status Code of the operation
		# result.message #=> Error message if it failed
		# result.value #=> nil
		#
		# @param [String] uri the URL of the blob
		# @return [OperationResult]
		def delete_blob_if_exist(uri)
			raise NotImplementedError uri
		end

		# Deletes the blob if it exist, else does nothing
		#
		# Raises exception if the operation fails
		#
		# uri = "https://host.com/folder/pic.png"
		# client.delete_blob! uri
		#
		# @param [String] uri the URL of the blob
		def delete_blob_if_exist!(uri)
			result = delete_blob_if_exist uri
			raise BucketOperationException.new(result) unless result.success
		end

	end

	# Client to interact with Storage service. Ability to perform Bucket CRUD
	class Client
		include BucketMethods, BlobMethods
	end
end
