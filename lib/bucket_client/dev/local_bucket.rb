require "pathname"
require "bucket_client/operation_result"

module BucketClient
	class LocalBucket < Bucket
		attr_reader :key

		def initialize(parent, key)
			@key = key
			@bucket_client = parent
		end

		def get_uri(key)
			combine @bucket_client.principal, @key, key
		end

		def get_blob_with_uri(uri)
			@bucket_client.get_blob uri
		end

		def exist_blob_with_uri(uri)
			@bucket_client.exist_blob(uri)
		end

		def put_blob_with_uri(payload, uri)
			@bucket_client.put_blob payload, uri
		end

		def update_blob_with_uri(payload, uri)
			exist = exist_blob_with_uri uri
			if exist
				put_blob_with_uri payload, uri
			else
				OperationResult.new false, "Blob does not exist", nil, 400
			end
		end

		def delete_blob_with_uri(uri)
			@bucket_client.delete_blob(uri)
		end

		def delete_blob_if_exist_with_uri(uri)
			@bucket_client.delete_blob_if_exist(uri)
		end

		def get_blob(key)
			get_blob_with_uri(get_uri key)
		end

		def exist_blob(key)
			exist_blob_with_uri(get_uri key)
		end

		def put_blob(payload, key)
			put_blob_with_uri(payload, get_uri(key))
		end

		def create_blob(payload, key)
			exist = exist_blob key
			if exist
				OperationResult.new false, "Blob already exist", nil, 400
			else
				put_blob payload, key
			end
		end

		def update_blob(payload, key)
			update_blob_with_uri(payload, get_uri(key))
		end

		def delete_blob(key)
			delete_blob_with_uri(get_uri key)
		end

		def delete_blob_if_exist(key)
			delete_blob_if_exist_with_uri(get_uri key)
		end

		private

			def combine(*args)
				Pathname.new(File.join(args)).cleanpath.to_s
			end


	end
end