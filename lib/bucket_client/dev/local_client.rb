require "fileutils"
require "bucket_client/dev/local_bucket"
require "bucket_client/operation_result"
require "addressable/uri"
require "pathname"


module BucketClient
	class LocalClient < Client

		attr_reader :path, :principal

		def initialize(path, principal = nil)
			@path = path
			@principal = principal || path
			unless File.directory? path
				FileUtils.mkdir_p path
			end
		end

		# @param [String] key
		def exist_bucket(key)
			raise ArgumentError.new("Cannot contain slash") if key.count("/") > 0 || key.count("\\") > 0
			File.directory? bucket_dir(key)
		end

		def get_bucket!(key)
			LocalBucket.new(self, key)
		end

		def put_bucket(key)
			val = get_bucket! key
			dir = bucket_dir key
			FileUtils.mkdir_p dir unless File.directory? dir
			OperationResult.new(true, "OK", val, 200)
		end

		def create_bucket(key)
			exist = exist_bucket key
			if exist
				OperationResult.new(false, "Bucket already exist", nil, 409)
			else
				put_bucket key
			end
		end

		def delete_bucket_if_exist(key)
			dir = bucket_dir key
			FileUtils.remove_dir dir if File.directory? dir
			OperationResult.new(true, "OK", nil, 204)
		end

		def delete_bucket(key)
			exist = exist_bucket key
			if exist
				delete_bucket_if_exist key
			else
				OperationResult.new(false, "Bucket does not exist", nil, 404)
			end
		end

		def set_read_policy(key, access)
			raise ArgumentError.new("Read Policy not accepted") if access != :public && access != :private
			exist = exist_bucket key
			OperationResult.new(exist, "", nil, exist ? 200 : 404)
		end

		def set_get_cors(key, _)
			exist = exist_bucket key
			OperationResult.new(exist, "", nil, exist ? 200 : 404)
		end

		def get_blob(uri)
			begin
				path = get_blob_path uri
				bin = IO.binread path
				OperationResult.new(true, "OK", bin, 200)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def exist_blob(uri)
			path = get_blob_path uri
			Pathname.new(path).exist?
		end

		def put_blob(payload, uri)
			begin
				path = get_blob_path uri
				dir = File.dirname path
				FileUtils.mkdir_p dir unless File.directory? dir
				IO.binwrite path, payload
				OperationResult.new(true, "OK", uri, 204)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def update_blob(payload, uri)
			exist = exist_blob uri
			if exist
				put_blob payload, uri
			else
				OperationResult.new(false, "Blob does not exist", nil, 404)
			end
		end

		def delete_blob_if_exist(uri)
			begin
				path = get_blob_path uri
				FileUtils.rm_f path if exist_blob uri
				OperationResult.new(true, "Deleted", nil, 204)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def delete_blob(uri)
			exist = exist_blob uri
			if exist
				delete_blob_if_exist uri
			else
				OperationResult.new(false, "Bucket does not exist", nil, 404)
			end
		end

		private

			def get_blob_path(uri)
				data = break_uri uri
				blob_dir data[:bucket], data[:blob]
			end

			def break_uri(uri)
				url = Addressable::URI.parse uri
				path = ("/" + url.path).split(principal).drop(1).join(principal)
				arr = path.split "/"
				{:bucket => arr[0], :blob => arr.drop(1).join("/")}
			end

			def bucket_dir(key)
				combine(@path, key)
			end

			def blob_dir(bucket, key)
				combine(@path, bucket, key)
			end

			def combine(*args)
				Pathname.new(File.join(args)).cleanpath.to_s
			end
	end
end