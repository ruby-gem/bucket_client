class OperationResult

	def initialize(success, message, value, code)
		@success = success
		@message = message
		@value = value
		@code = code
	end

	# Message from the server of the operation. Failure message will be here too
	# @return [String]
	def message
		@message
	end

	# HTTP Status code of the operation.
	# @return [Integer]
	def code
		@code
	end

	# Whether the operation was successful
	# @return [Boolean]
	def success
		@success
	end

	# Value of the operation result if it has a value.
	# Can be uri or byte array
	def value
		@value
	end
end