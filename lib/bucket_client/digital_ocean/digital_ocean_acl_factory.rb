class DigitalOceanACLFactory
	def generate_acl(access, owner_id)
		if access === :public
			"<AccessControlPolicy xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">
  <Owner>
    <ID>#{owner_id}</ID>
  </Owner>
  <AccessControlList>
    <Grant>
      <Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"CanonicalUser\">
        <ID>#{owner_id}</ID>
      </Grantee>
      <Permission>FULL_CONTROL</Permission>
    </Grant>
    <Grant>
      <Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"Group\">
        <URI>http://acs.amazonaws.com/groups/global/AllUsers</URI>
      </Grantee>
      <Permission>READ</Permission>
    </Grant>
  </AccessControlList>
</AccessControlPolicy>"
		else
			"<AccessControlPolicy xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">
  <Owner>
    <ID>#{owner_id}</ID>
  </Owner>
  <AccessControlList>
    <Grant>
      <Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"CanonicalUser\">
        <ID>#{owner_id}</ID>
      </Grantee>
      <Permission>FULL_CONTROL</Permission>
    </Grant>
  </AccessControlList>
</AccessControlPolicy>"
		end
	end
end