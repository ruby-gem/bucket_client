class BucketOperationException < StandardError

	# Create a BucketOperationException
	# @param [OperationResult] result the operation that failed
	def initialize(result)
		super("Bucket Operation failed: #{result.message}. Status Code: #{result.code}")
	end
end