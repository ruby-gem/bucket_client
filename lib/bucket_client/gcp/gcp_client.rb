require "google/cloud/storage"
require "mimemagic"
require "bucket_client/operation_result"
require "bucket_client/gcp/gcp_bucket"

module BucketClient
	class GCPClient < Client
		def initialize(project_id, secret)
			@client = Google::Cloud::Storage.new(project_id: project_id, keyfile: secret)
		end

		def exist_bucket(key)
			!@client.bucket(key).nil?
		end

		def get_bucket!(key)
			GCPBucket.new(self, key)
		end

		def put_bucket(key)
			exist = exist_bucket key
			if exist
				bucket = get_bucket! key
				OperationResult.new(true, "OK", bucket, 200)
			else
				create_bucket key
			end
		end

		def create_bucket(key)
			begin
				@client.create_bucket key
				bucket = get_bucket! key
				OperationResult.new(true, "OK", bucket, 200)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def delete_bucket(key)
			exist = exist_bucket key
			if exist
				bucket = @client.bucket(key)
				success = bucket.delete
				message = success ? "Deleted" : "Failed to delete"
				code = success ? 204 : 400
				OperationResult.new(success, message, nil, code)
			else
				OperationResult.new(false, "Bucket does not exist", nil, 404)
			end
		end

		def delete_bucket_if_exist(key)
			exist = exist_bucket key
			if exist
				delete_bucket key
			else
				OperationResult.new(true, "Bucket already deleted", nil, 200)
			end
		end

		def set_read_policy(key, access)
			raise ArgumentError.new("Read Policy not accepted") if access != :public && access != :private
			begin
				bucket = @client.bucket key
				modify_acl bucket, access
				bucket.files.each do |file|
					modify_acl file, access
				end
				OperationResult.new(true, "OK", nil, 200)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def set_get_cors(key, cors)
			begin

				bucket = @client.bucket key
				bucket.cors(&:clear)
				bucket.cors {|rules| rules.add_rule cors, "GET"}
				OperationResult.new(true, "OK", nil, 200)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def get_blob(uri)
			data = break_uri uri
			begin
				bucket = @client.bucket data[:bucket]
				blob = bucket.file data[:blob]
				bin = blob.download.read
				OperationResult.new(true, "OK", bin, 200)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def exist_blob(uri)
			data = break_uri uri
			bucket = @client.bucket data[:bucket]
			bucket.file(data[:blob], skip_lookup: true).exists?
		end

		def put_blob(payload, uri)
			mime = MimeMagic.by_magic payload
			data = break_uri uri
			begin
				# @type [Google::Cloud::Storage::Bucket]
				bucket = @client.bucket data[:bucket]
				f = StringIO.new
				f << payload
				type = mime&.type || "text/plain"
				file = bucket.create_file f, data[:blob], cache_control: "no-cache", content_type: type
				OperationResult.new(true, "OK", file.public_url, 200)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		def update_blob(payload, uri)
			exist = exist_blob uri
			if exist
				put_blob payload, uri
			else
				OperationResult.new(false, "Blob does not exist", nil, 404)
			end
		end

		def delete_blob_if_exist(uri)
			exist = exist_blob uri
			if exist
				delete_blob uri
			else
				OperationResult.new(true, "Blob already deleted", nil, 204)
			end
		end

		def delete_blob(uri)
			data = break_uri uri
			begin
				bucket = @client.bucket data[:bucket]
				bucket.file(data[:blob], skip_lookup: true).delete
				OperationResult.new(true, "Deleted Blob", nil, 204)
			rescue StandardError => e
				OperationResult.new(false, e.message, nil, 400)
			end
		end

		private

			def break_uri(uri)
				url = Addressable::URI.parse uri
				fragments = url.path.split("/").select {|x| !x.nil? && !x.empty?}
				bucket = fragments[0]
				blob = fragments.drop(1).join "/"
				{bucket: bucket, blob: blob}
			end

			def modify_acl(principal, access)
				if access == :private
					principal.acl.private!
					principal.default_acl.private!
				else
					principal.acl.public!
					principal.default_acl.public!
				end
			end

	end
end
