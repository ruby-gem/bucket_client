require "bucket_client/version"
require "bucket_client/operation_result"
require "bucket_client/bucket_operation_exception"
require "bucket_client/bucket"
require "bucket_client/client"
require "bucket_client/aws/aws_client"
require "bucket_client/digital_ocean/digital_ocean_client"
require "bucket_client/azure/azure_client"
require "bucket_client/gcp/gcp_client"
require "bucket_client/dev/local_client"
require "kirin_http"

module BucketClient

	# @param [Symbol] type the type of client to generate. Accepts
	# :local for local,
	# :aws for AWS S3 Bucket
	# :azure for Azure Blob Storage
	# :spaces for DigitalOcean Spaces
	# :gcp for Google Cloud Platform Storage Account
	# @param [String] id The id for the client. Only applicable to :aws, :azure, :spaces and :gcp
	# For GCP, this is your project id
	# For AWS, this is your access ID
	# For Digital Ocean spaces, this is your access ID
	# For Azure, this is your account name for your blob storage account
	# @param [String or Hash] secret The secret for the client. Only applicable to :aws, :azure, :spaces and :gcp
	# For GCP, this can either be a path to your secret.json, or it can be the JSON serialized as a hash.
	# For AWS, this is your secret key
	# For Digital Ocean spaces, this is your secret key
	# For Azure, this is your secret key
	# @param [String] region The region of your bucket. Only applicable to :aws and :spaces
	# For AWS, this is your region string. eg: ap-southeast-1
	# For Digital Ocean Spaces, this is your region string. ep: sgp1
	# @param [String] path The local path for mocking a bucket (using disk). Only applicable to :local
	# @param [String] principal The public path of the bucket. Will use value of "path" if not provided. Only applicable to :local
	# @return [BucketClient::Client]
	def BucketClient.generate(type: :local, id: nil, secret: nil, region: nil, path: nil, principal: nil)
		http = KirinHttp::BasicClient.new
		case type
			when :local
				raise ArgumentError.new("Path not provided") if path.nil?
				LocalClient.new(path, principal)
			when :aws
				raise ArgumentError.new("id not provided") if id.nil?
				raise ArgumentError.new("secreted not provided") if secret.nil?
				raise ArgumentError.new("region not provided") if region.nil?
				AWSClient.new(http, id, secret, region)
			when :azure
				raise ArgumentError.new("account name/id not provided") if id.nil?
				raise ArgumentError.new("secret not provided") if secret.nil?
				AzureClient.new(id, secret)
			when :spaces
				raise ArgumentError.new("id not provided") if id.nil?
				raise ArgumentError.new("secreted not provided") if secret.nil?
				raise ArgumentError.new("region not provided") if region.nil?
				DigitalOceanClient.new(http, id, secret, region)
			when :gcp
				raise ArgumentError.new("project id not provided") if id.nil?
				raise ArgumentError.new("secret not provided") if secret.nil?
				GCPClient.new(id, secret)
			else
				raise ArgumentError.new("Unknown Client type: " + type.to_s)
		end
	end

end