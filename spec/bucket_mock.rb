class BucketMock < BucketClient::Bucket


	def get_blob(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", [1, 1, 1, 1], 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def get_blob_with_uri(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", [1, 1, 1, 1], 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def delete_blob(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def delete_blob_with_uri(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def delete_blob_if_exist(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def delete_blob_if_exist_with_uri(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def create_blob(payload, key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", "https://#{key}", 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def update_blob(payload, key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", "https://#{key}", 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def update_blob_with_uri(payload, key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", key, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def put_blob(payload, key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", "https://#{key}", 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def put_blob_with_uri(payload, key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", key, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end


end