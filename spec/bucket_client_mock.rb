class BucketClientMock < BucketClient::Client

	def create_bucket(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", BucketMock.new, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def delete_bucket(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def delete_bucket_if_exist(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def set_read_policy(key, access)
		raise ArgumentError.new("Read Policy not accepted") if access != :public && access != :private
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def set_get_cors(key, cors)
		if key.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def put_bucket(key)
		if key.length == 2
			OperationResult.new(true, "Succeeded", BucketMock.new, 200)
		else
			OperationResult.new(false, "Wrong key", nil, 400)
		end
	end

	def exist_bucket(key)
		key.length == 2
	end

	def get_bucket!(key)
		BucketMock.new
	end

	def get_blob(uri)
		if uri.length == 2
			OperationResult.new(true, "Succeeded", [1, 2, 3, 4], 200)
		else
			OperationResult.new(false, "Wrong uri", nil, 400)
		end
	end

	def update_blob(payload, uri)
		if uri.length == 2
			OperationResult.new(true, "Succeeded", uri, 200)
		else
			OperationResult.new(false, "Wrong uri", nil, 400)
		end
	end

	def put_blob(payload, uri)
		if uri.length == 2
			OperationResult.new(true, "Succeeded", uri, 200)
		else
			OperationResult.new(false, "Wrong uri", nil, 400)
		end
	end

	def delete_blob(uri)
		if uri.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong uri", nil, 400)
		end
	end

	def delete_blob_if_exist(uri)
		if uri.length == 2
			OperationResult.new(true, "Succeeded", nil, 200)
		else
			OperationResult.new(false, "Wrong uri", nil, 400)
		end
	end

end