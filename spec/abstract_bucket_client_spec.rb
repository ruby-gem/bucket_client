require 'rspec'
require "bucket_client_mock"

describe BucketClient::Client do
	let(:client) {BucketClientMock.new}
	describe BucketClient::BucketMethods do
		describe 'create_bucket!' do
			it 'should return uri if succeed' do
				expect((client.create_bucket! "ab").class).to eq BucketMock
			end
			it 'should raise exception if fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {client.create_bucket! "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'delete_bucket!' do
			it 'should return nil if succeed' do
				expect(client.delete_bucket! "ab").to be_nil
			end

			it 'should raise exception if fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {client.delete_bucket! "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'set_read_policy!' do
			it 'should return nil if succeed' do
				expect(client.set_read_policy! "ab", :public).to be_nil
			end
			it 'should raise ArgumentError if input neither :public nor :private' do
				error = "Read Policy not accepted"
				expect {client.set_read_policy! "ab", :protected}.to raise_error(ArgumentError, error)
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {client.set_read_policy! "abc", :private}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'set_get_cors!' do
			it 'should return nil if succeed' do
				expect(client.set_get_cors! "ab", [1, 2, 3, 4]).to be_nil
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {client.set_get_cors! "abc", :private}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'put_bucket' do
			it 'should return bucket if it succeeds' do
				expect(client.put_bucket!("ab").class).to eq BucketMock
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {client.put_bucket! "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'delete_bucket_if_exist!' do
			it 'should return nil if it succeeds' do
				expect(client.delete_bucket! "ab").to be_nil
			end
			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {client.delete_bucket! "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'get_bucket' do
			it 'should return Bucket if it succeeds' do
				expect(client.get_bucket("ab").class).to eq BucketMock
			end
			it 'should raise exception if bucket does not exist' do
				error = "Bucket Operation failed: Bucket does not exist. Status Code: 404"
				expect {client.get_bucket "abc"}.to raise_error(BucketOperationException, error)
			end

		end

	end

	describe BucketClient::BlobMethods do
		let(:error) {"Bucket Operation failed: Wrong uri. Status Code: 400"}
		describe 'get_blob!' do
			it 'should return blob if succeeds' do
				expect(client.get_blob! "ab").to eq [1, 2, 3, 4]
			end
			it 'should raise exception if fails' do
				expect {client.get_blob! "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'update_blob!' do
			it 'should return uri if succeeds' do
				expect(client.update_blob! nil, "ab").to eq "ab"
			end
			it 'should raise exception if fails' do
				expect {client.update_blob! nil, "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'put_blob!' do
			it 'should return uri if succeeds' do
				expect(client.put_blob! nil, "ab").to eq "ab"
			end
			it 'should raise exception if fails' do
				expect {client.update_blob! nil, "abc"}.to raise_error BucketOperationException, error
			end
		end

		describe 'delete_blob!' do
			it 'should return nil if succeeds' do
				expect(client.delete_blob! "ab").to be_nil
			end

			it 'should raise exception if it fails' do
				expect {client.delete_blob! "abc"}.to raise_error BucketOperationException, error
			end
		end

		describe 'delete_blob_if_exist' do
			it 'should return nil if succeeds' do
				expect(client.delete_blob_if_exist! "ab").to be_nil
			end
			it 'should raise exception if it fails' do
				expect {client.delete_blob_if_exist! "abc"}.to raise_error BucketOperationException, error
			end
		end
	end

end