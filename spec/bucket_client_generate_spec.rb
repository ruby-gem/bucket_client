require 'rspec'
require 'json'

describe 'generate' do


	describe 'AWS generation' do
		id = ENV["AWS_ID"]
		secret = ENV["AWS_SECRET"]
		region = ENV["AWS_REGION"]

		it 'should generate AWS client if the :aws was type and credentials properly filled in' do
			client = BucketClient::generate type: :aws, id: id, secret: secret, region: region
			expect(client.class).to eq BucketClient::AWSClient
		end

		it 'should raise exception if id was not provided' do
			expect {BucketClient::generate type: :aws, secret: secret, region: region}.to raise_error(ArgumentError, "id not provided")
		end

		it 'should raise exception if secret was not provided' do
			expect {BucketClient::generate type: :aws, id: id, region: region}.to raise_error(ArgumentError, "secreted not provided")
		end

		it 'should raise exception if region was not provided' do
			expect {BucketClient::generate type: :aws, id: id, secret: secret}.to raise_error(ArgumentError, "region not provided")
		end
	end

	describe "Local generation" do
		it 'should generate local Client if :local was type' do
			client = BucketClient::generate type: :local, path: "sample-host"
			expect(client.class).to eq BucketClient::LocalClient
		end

		it 'should raise exception if path is not provided' do
			expect {BucketClient::generate type: :local}.to raise_error ArgumentError, "Path not provided"
		end
	end

	describe "Azure generation" do
		it 'should generate azure client if :azure was type with credential provided' do
			client = BucketClient::generate type: :azure, id: ENV["AZURE_ACC_ID"], secret: ENV["AZURE_KEY"]
			expect(client.class).to eq BucketClient::AzureClient
		end

		it 'should raise exception if id is not provided' do
			expect {BucketClient::generate type: :azure, secret: ENV["AWS_SECRET"]}.to raise_error ArgumentError, "account name/id not provided"
		end

		it 'should raise exception if id is not provided' do
			expect {BucketClient::generate type: :azure, id: ENV["AZURE_ACC_ID"]}.to raise_error ArgumentError, "secret not provided"
		end
	end

	describe "Digital Ocean generation" do
		id = ENV["DO_ID"]
		secret = ENV["DO_SECRET"]
		region = ENV["DO_REGION"]

		it 'should generate Digital Ocean client if the :spaces was type and credentials properly filled in' do
			client = BucketClient::generate type: :spaces, id: id, secret: secret, region: region
			expect(client.class).to eq BucketClient::DigitalOceanClient
		end

		it 'should raise exception if id was not provided' do
			expect {BucketClient::generate type: :spaces, secret: secret, region: region}.to raise_error(ArgumentError, "id not provided")
		end

		it 'should raise exception if secret was not provided' do
			expect {BucketClient::generate type: :spaces, id: id, region: region}.to raise_error(ArgumentError, "secreted not provided")
		end

		it 'should raise exception if region was not provided' do
			expect {BucketClient::generate type: :spaces, id: id, secret: secret}.to raise_error(ArgumentError, "region not provided")
		end
	end

	describe "GCP generation" do
		id = ENV["GOOGLE_ID"]
		secret = JSON.parse ENV["GOOGLE_KEY"]
		it 'should generate GCP client if type is :gcp and credentials are correct' do
			client = BucketClient::generate type: :gcp, id: id, secret: secret
			expect(client.class).to eq BucketClient::GCPClient
		end

		it 'should raise exception if id is not provided' do
			expect {BucketClient::generate type: :gcp, secret: secret}.to raise_error ArgumentError, "project id not provided"
		end
		it 'should raise exception if secret is not provided' do
			expect {BucketClient::generate type: :gcp, id: id}.to raise_error ArgumentError, "secret not provided"
		end
	end

	it 'should raise exception if type is unknown' do
		expect {BucketClient::generate type: :aliyun}.to raise_error ArgumentError, "Unknown Client type: aliyun"
	end

end