require "bucket_mock"
describe BucketClient::Bucket do
	let(:bucket) {BucketMock.new}

	describe BucketClient::KeyMethod do
		describe 'get_blob!' do
			it 'should should return binary if success' do
				bin = bucket.get_blob! 'ab'
				expect(bin).to eq([1, 1, 1, 1])
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.get_blob! 'a'}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'create_blob!' do
			it 'should return uri if succeeds' do
				expect(bucket.create_blob! nil, "ab").to eq("https://ab")
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.create_blob! nil, "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'update_blob!' do
			it 'should return uri if succeeds' do
				expect(bucket.update_blob! nil, "ab").to eq("https://ab")
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.update_blob! nil, "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'put_blob!' do
			it 'should return uri if succeeds' do
				expect(bucket.put_blob! nil, "ab").to eq("https://ab")
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.put_blob! nil, "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'delete_blob!' do
			it 'should return nil if succeeds' do
				expect(bucket.delete_blob! "ab").to be_nil
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.delete_blob! "abc"}.to raise_error(BucketOperationException, error)
			end
		end
		describe 'delete_blob_if_exist!' do
			it 'should return nil if succeeds' do
				expect(bucket.delete_blob_if_exist! "ab").to be_nil
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.delete_blob_if_exist! "abc"}.to raise_error(BucketOperationException, error)
			end
		end
	end

	describe BucketClient::UriMethod do
		describe 'get_blob_with_uri!' do
			it 'should should return binary if success' do
				bin = bucket.get_blob_with_uri! 'ab'
				expect(bin).to eq([1, 1, 1, 1])
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.get_blob_with_uri! 'a'}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'update_blob_with_uri!' do
			it 'should return uri if succeeds' do
				expect(bucket.update_blob_with_uri! nil, "ab").to eq("ab")
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.update_blob_with_uri! nil, "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'put_blob_with_uri!' do
			it 'should return uri if succeeds' do
				expect(bucket.put_blob_with_uri! nil, "ab").to eq("ab")
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.put_blob_with_uri! nil, "abc"}.to raise_error(BucketOperationException, error)
			end
		end

		describe 'delete_blob_with_uri!' do
			it 'should return nil if succeeds' do
				expect(bucket.delete_blob_with_uri! "ab").to be_nil
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.delete_blob_with_uri! "abc"}.to raise_error(BucketOperationException, error)
			end
		end
		describe 'delete_blob_if_exist_with_uri!' do
			it 'should return nil if succeeds' do
				expect(bucket.delete_blob_if_exist_with_uri! "ab").to be_nil
			end

			it 'should raise exception if it fails' do
				error = "Bucket Operation failed: Wrong key. Status Code: 400"
				expect {bucket.delete_blob_if_exist_with_uri! "abc"}.to raise_error(BucketOperationException, error)
			end
		end
	end

end