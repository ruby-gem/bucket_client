lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "bucket_client/version"

Gem::Specification.new do |spec|
	spec.name = "bucket_client"
	spec.version = BucketClient::VERSION
	spec.authors = ["kirinnee"]
	spec.email = ["kirinnee97@gmail.com"]

	spec.summary = %q{A gem that allows for ruby code to interact with cloud blob storage such as AWS S3, GCP Cloud
Storage and Azure Blob Storage}
	spec.description = %q{Bucket Client is a ruby gem that allows programmers to interact with popular Blob Storage cloud
services. This intends to act as a layer of abstraction, much like ORM is to databases.

With this, you may easily change the blob storage provider or even defer them.

The supported cloud storage include:
- Google Cloud Platform Cloud Storage
- Amazon Web Service S3 Bucket
- Digital Ocean Spaces
- Azure Blob Storage (Microsoft).}
	spec.homepage = "https://gitlab.com/ruby-gem/bucket_client"
	spec.license = "MIT"

	# Specify which files should be added to the gem when it is released.
	# The `git ls-files -z` loads the files in the RubyGem that have been added into git.
	spec.files = Dir.chdir(File.expand_path('..', __FILE__)) do
		`git ls-files -z`.split("\x0").reject {|f| f.match(%r{^(test|spec|features)/})}
	end
	spec.bindir = "exe"
	spec.executables = spec.files.grep(%r{^exe/}) {|f| File.basename(f)}
	spec.require_paths = ["lib"]

	spec.add_dependency "kirin_http"
	spec.add_dependency "mimemagic"
	spec.add_dependency "ox"
	spec.add_dependency "azure-storage-blob"
	spec.add_dependency 'addressable', '~> 2.5'
	spec.add_dependency 'google-cloud-storage', '~> 1.17'

	spec.add_development_dependency "bundler", "~> 1.17"
	spec.add_development_dependency "rake", "~> 10.0"
	spec.add_development_dependency "rspec", "~> 3.0"
	spec.add_development_dependency "dotenv"
end
